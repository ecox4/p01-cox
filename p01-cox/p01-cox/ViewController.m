//
//  ViewController.m
//  p01-cox
//
//  Created by Em on 1/23/17.
//
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize label;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)changeMessage{
    //hints for changing the bg color taken from stackoverflow.com/questions/1998304/how-to-change-the-background-color-of-the-viewcontroller-in-objective-c
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    if(_numPress%4 == 0){
        [label setText:@"Don't press the button"];
        [label setFont:[UIFont fontWithName:@"MarkerFelt-Thin" size:11.0]];
    }else if((_numPress < 10)&&(_numPress%4 == 2)){
        [label setFont:[UIFont fontWithName:@"Helvetica-Bold" size:7.0]];
        [label setTextColor:[UIColor redColor]];
    }else if(_numPress == 10){
        [label setText:@"You really shouldn't press the button"];
        [label setFont:[UIFont fontWithName:@"Courier" size:12]];
        [label setTextColor:[UIColor greenColor]];
        self.view.backgroundColor = [UIColor blackColor];
    }else if(_numPress%4 == 1){
        [label setText:@"Goodbye world"];
        [label setTextColor:[UIColor blueColor]];
    }else if(_numPress%4 == 3){
        [label setText:@"World hello"];
    }else{
        [label setText:@"World goodbye"];
    }
    
    if(_numPress > 10){
        exit(0);
    }
    
    _numPress++;
}
@end
